//
//  DetailPresenter.swift
//  testApp
//
//  Created by Архип on 11/26/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

class DetailPresenter: NSObject, DetailViewPresenter, NewsTableViewCellDelegate {
    
    
    
    private var cellStates: [CellState]?
    private var items: [Item]?
    
    var channel: Channel
    var splitPresenter: SplitPresenter
    var view: DetailViewProtocol
    
    
    required init(view: DetailViewProtocol, channel: Channel, split: SplitPresenter) {
        self.channel = channel
        self.view = view
        self.splitPresenter = split
        self.items = channel.items?.allObjects as? [Item]
        self.cellStates = Array(repeating: .collapsed, count: self.items?.count ?? 0)
        
        super.init()
        
        self.fetchData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channel.items?.allObjects.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NewsTableViewCell
        if let item = items?[indexPath.item] {
            cell.titleLabel.text = item.title
            cell.descriptionLabel.text = item.descript
            cell.dateLabel.text = item.pubDate
            cell.selectionStyle = .none
            cell.delegate = self
            cell.itemImageView.image = nil
            if let url = URL(string:item.imageUrl ?? "") {
                cell.itemImageView.sd_setImage(with: url, completed: nil)
            }
            if let cellStates = cellStates {
                cell.descriptionLabel.numberOfLines = (cellStates[indexPath.row] == .expanded) ? 0 : 4

            }
        }
        
        return cell
    }
    
    
    private func fetchData()
    {
        //http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml
        //https://developer.apple.com/news/rss/news.rss
        //https://www.liga.net/biz/all/rss.xml
        let feedParser = FeedParser()
        if channel.url == nil {
            return
        }
        feedParser.parseFeed(url: channel.url!) { (rssItems) in
            
            OperationQueue.main.addOperation {
                let db = CoreDataStack.shared
                if let oldItems = self.channel.items?.allObjects as? [Item] {
                    db.deleteObjects(oldItems)
                }
                var newItems = [Item]()
                
                (0..<rssItems.count).forEach({ (index) in
                    let newItem = db.createObject(Item.self)
                    newItem.priority = Int64(index)
                    newItem.fillFrom(rssItems[index])
                    newItems.append(newItem)
                })
                
                self.channel.items = NSSet(array: newItems)
                self.items = newItems
                db.saveContext()
                self.cellStates = Array(repeating: .collapsed, count: rssItems.count)
                self.view.tableView?.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! NewsTableViewCell
        
        tableView.beginUpdates()
        cell.descriptionLabel.numberOfLines = (cell.descriptionLabel.numberOfLines == 0) ? 3 : 0
        
        cellStates?[indexPath.row] = (cell.descriptionLabel.numberOfLines == 0) ? .expanded : .collapsed
        
        tableView.endUpdates()
    }
    
    func openPressed(sender: NewsTableViewCell) {
        if let index = self.view.tableView.indexPath(for: sender), let url = URL(string: items?[index.row].link ?? ""){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}

extension Item {
    func fillFrom(_ rItem: RSSItem){
        self.title = rItem.title
        self.link = rItem.link
        self.pubDate = rItem.pubDate
        self.descript = rItem.description
        self.imageUrl = rItem.imageUrl
    }
}
