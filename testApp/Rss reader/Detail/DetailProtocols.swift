//
//  DetailProtocols.swift
//  testApp
//
//  Created by Архип on 11/26/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

protocol DetailViewProtocol:class{
    var tableView:UITableView! {set get}
}

protocol DetailViewPresenter: UITableViewDelegate, UITableViewDataSource {
    init(view: DetailViewProtocol, channel: Channel, split: SplitPresenter)
}
