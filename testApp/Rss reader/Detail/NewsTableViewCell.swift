//
//  NewsTableViewCell.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import UIKit

enum CellState {
    case expanded
    case collapsed
}
protocol NewsTableViewCellDelegate: class {
    func openPressed(sender:NewsTableViewCell)
}

class NewsTableViewCell: UITableViewCell {
    weak var delegate:NewsTableViewCellDelegate?
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var descriptionLabel:UILabel! {
        didSet {
            descriptionLabel.numberOfLines = 3
        }
    }
    @IBOutlet weak var dateLabel:UILabel!

    @IBOutlet weak var button: UIButton!
    
    @IBAction func openAction(_ sender: Any) {
        self.delegate?.openPressed(sender:self)
    }
}












