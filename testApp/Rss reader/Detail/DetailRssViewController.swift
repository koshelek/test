//
//  DetailRssViewController.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import UIKit
fileprivate let SelfId = "DetailRssViewController"

class DetailRssViewController: UIViewController, DetailViewProtocol {
    @IBOutlet weak var tableView: UITableView!
    var presenter: DetailViewPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 155.0
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if presenter != nil {
            tableView.dataSource = self.presenter
            tableView.delegate = self.presenter
        }
        
    }

}

extension DetailRssViewController: StoryboardInitable {
    static func initFromStoryboard() -> UIViewController {
        let vc = UIStoryboard(name: startSt, bundle: nil).instantiateViewController(withIdentifier: SelfId) as! DetailRssViewController
        return vc
    }
}
