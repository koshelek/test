//
//  MasterProtocols.swift
//  testApp
//
//  Created by Архип on 11/25/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

protocol MasterView: class{
    func updateData()
}

protocol MasterViewPresenter: DataBaseUpdate, UITableViewDataSource {
    init(view: MasterView, channels: [Channel], split: SplitPresenter)
    func checkForUpdates()
    func addChannel(_ name: String, _ address: String) -> Bool
    func deleteChannelAt(_ index: Int)
    func didSelectAt(_ index: Int)
    
}
