//
//  MasterPresenter.swift
//  testApp
//
//  Created by Архип on 11/25/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

class MasterPresenter:NSObject, MasterViewPresenter {
    unowned var view: MasterView
    unowned var splitPresenter : SplitPresenter
    var channels: [Channel]
    required init(view: MasterView, channels: [Channel], split: SplitPresenter) {
        self.view = view
        self.channels = channels
        self.splitPresenter = split
    }
    
    func checkForUpdates() {
        dbUpdated()
    }
    
    func dbUpdated() {
//        let predicate = NSPredicate(format: "\(#keyPath(Channel.self))")
        let all = CoreDataStack.shared.getAllObjects(Channel.self)
        channels = all.sorted(){$0.name ?? "" > $1.name ?? ""}
        view.updateData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        cell.textLabel?.text = channels[indexPath.row].name
        cell.detailTextLabel?.text = channels[indexPath.row].url
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteChannelAt(indexPath.row)
        }
    }
    
    func didSelectAt(_ index: Int){
        let channel = channels[index]
        splitPresenter.showDetailFor(channel)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
    }
    
    func deleteChannelAt(_ index: Int){
        CoreDataStack.shared.deleteObject(channels[index])
        CoreDataStack.shared.saveContext()
        dbUpdated()
    }
    
    func addChannel(_ name: String, _ address: String) -> Bool{
        guard let url = URL(string: address), name.isEmpty == false  else {
            return false
        }
        let channel = CoreDataStack.shared.createObject(Channel.self)
        channel.name = name
        channel.url = url.absoluteString
        CoreDataStack.shared.saveContext()
        dbUpdated()
        return true
    }
    
    
    
}
