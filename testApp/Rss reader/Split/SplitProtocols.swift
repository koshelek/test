//
//  SplitProtocols.swift
//  testApp
//
//  Created by Архип on 11/25/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

protocol SplitView: class{

    func setVCs(_ vcs: [UIViewController])
}

protocol SplitViewPresenter {
    init(view: SplitView)
    func viewWillAppear()
    func showDetailFor(_ channel: Channel)
}
