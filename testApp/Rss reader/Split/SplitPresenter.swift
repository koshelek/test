//
//  SplitPresnter.swift
//  testApp
//
//  Created by Архип on 11/25/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

class SplitPresenter: SplitViewPresenter {
    unowned var view: SplitView
    
    required init(view: SplitView){
        self.view = view
        self.viewWillAppear()
    }
    
    func viewWillAppear(){
        
        let masterView = MasterRssTableViewController.initFromStoryboard() as! MasterRssTableViewController
        let masterVC = UINavigationController(rootViewController: masterView)
        let presenter = MasterPresenter(view: masterView, channels: [Channel](), split: self)
        masterView.presenter = presenter

        let detailView = DetailRssViewController.initFromStoryboard() as! DetailRssViewController
        let detailVC = UINavigationController(rootViewController: detailView)
        let channel = (CoreDataStack.shared.getObjects(Channel.self, predicate: nil).first) ?? CoreDataStack.shared.createObject(Channel.self)
        let _presenter = DetailPresenter(view: detailView, channel: channel, split: self)
        detailView.presenter = _presenter
 
        self.view.setVCs([masterVC, detailVC])
    }
    
    func showDetailFor(_ channel: Channel){
        if let vc = view as? UISplitViewController {
            let detailView = DetailRssViewController.initFromStoryboard() as! DetailRssViewController
            let detailVC = UINavigationController(rootViewController: detailView)
            let presenter = DetailPresenter(view: detailView, channel: channel, split: self)
            detailView.presenter = presenter
            vc.showDetailViewController(detailVC, sender: self)
        }
    }
    
}
