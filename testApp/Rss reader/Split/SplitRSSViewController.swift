//
//  SplitRSSViewController.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import UIKit
fileprivate let SelfId = "SplitRSSViewController"

class SplitRSSViewController: UISplitViewController, SplitView {
    var presenter: SplitViewPresenter? {
        didSet {
            self.presenter?.viewWillAppear()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.viewWillAppear()
    }
    
    func setVCs(_ vcs: [UIViewController]){

        preferredDisplayMode = .allVisible
        viewControllers = vcs
        
    }
    
}

extension SplitRSSViewController: StoryboardInitable {
    static func initFromStoryboard() -> UIViewController {
        let vc = UIStoryboard(name: startSt, bundle: nil).instantiateViewController(withIdentifier: SelfId) as! SplitRSSViewController
        return vc
    }
}
