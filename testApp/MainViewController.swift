//
//  MainViewController.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import UIKit

let firstLaunchKey = "firstLaunchKey"

protocol MainView {
    
}

protocol MainViewPresenter {
    init(view: MainView)
    func getViews()->[UIViewController]
}

class MainPresenter: MainViewPresenter {
    let view: MainView
    required init(view: MainView) {
        self.view = view
        let df = UserDefaults.standard
        let db = CoreDataStack.shared
        
        if df.bool(forKey: firstLaunchKey) == false {
            if db.getAllObjects(City.self).first == nil {
                let city1 = db.createObject(City.self)
                city1.id = "2643743"
                city1.name = "London"
                city1.country = db.createObject(Country.self)
                city1.country?.iso = "GB"
                city1.priority = 0
                city1.showWheather = true
                let city2 = db.createObject(City.self)
                city2.id = "702550"
                city2.name = "Lviv"
                city2.country = db.createObject(Country.self)
                city2.country?.iso = "UA"
                city2.priority = 1
                city2.showWheather = true
            }

            if db.getAllObjects(Channel.self).first == nil {
                let channel1 = db.createObject(Channel.self)
                channel1.url = "https://developer.apple.com/news/rss/news.rss"
                channel1.name = "Apple news"
                let channel2 = db.createObject(Channel.self)
                channel2.url = "http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml"
                channel2.name = "Nytimes news"
                let channel3 = db.createObject(Channel.self)
                channel3.url = "https://www.liga.net/biz/all/rss.xml"
                channel3.name = "Liga news"
            }
            db.saveContext()
            df.set(true, forKey: firstLaunchKey)
        }
    }
    
    func getViews()->[UIViewController]{
    
        //1
        let splitView = SplitRSSViewController()
        splitView.tabBarItem = UITabBarItem(title: "RSS reader", image: nil, selectedImage: nil)
        let splitPresenter = SplitPresenter(view: splitView)
        splitView.presenter = splitPresenter
        
        //2
        let wheatherView = WheatherViewController.initFromStoryboard() as! WheatherViewController
        let weatherVC = UINavigationController(rootViewController: wheatherView)
        wheatherView.tabBarItem = UITabBarItem(title: "Wheather", image: nil, selectedImage: nil)
        wheatherView.tabBarItem.title = "Wheather"
        let predicate = NSPredicate(format: "\(#keyPath(City.showWheather)) = YES")
        let cities = CoreDataStack.shared.getObjects(City.self, predicate: predicate).sorted {$0.priority > $1.priority}
        let wheatherPresenter = WheatherPresenter(view: wheatherView, cities: cities)
        wheatherView.presenter = wheatherPresenter

        
        return [splitView, weatherVC]
    }
}

class MainViewController: UITabBarController, MainView {
    var presenter: MainViewPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewControllers=self.presenter?.getViews()
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
