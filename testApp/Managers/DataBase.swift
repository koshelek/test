//
//  DataBase.swift
//  eProducts
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import CoreData

let ModelName = "DataModel"
let UpdateScreenNotification = "UpdateScreen"

let FirstLaunchKey = "FirstLaunchKey"

extension NSObject {
    var className: String {
        return String(describing: type(of: self)).components(separatedBy: ".").last!
    }
    
    class var className: String {
        return String(describing: self).components(separatedBy: ".").last!
    }
}


final class CoreDataStack{
    //    MARK: Properties

    static let shared = CoreDataStack(modelName:ModelName)
    private var storeName: String = ModelName
    private lazy var storeContainer :NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.storeName)
        container.persistentStoreDescriptions = [self.storeDescription]
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let nserror = error as NSError?{
                print ("Unresolved error \(nserror), \(nserror.userInfo)")
            }else{
            
            }
        })
        return container
    }()
    lazy var managedContext:NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    var refreshingDB: Bool = false
    
    init(modelName:String) {
        self.storeName = modelName
        setupSaveNotification()
    }
    
    var savingContext: NSManagedObjectContext {
        return storeContainer.newBackgroundContext()
    }
    
    var storeURL : URL {
        let storePaths = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)
        let storePath = storePaths[0] as NSString
        let fileManager = FileManager.default
        
        do {
            try fileManager.createDirectory(
                atPath: storePath as String,
                withIntermediateDirectories: true,
                attributes: nil)
        } catch {
            print("Error creating storePath \(storePath): \(error)")
        }
        
        let sqliteFilePath = storePath
            .appendingPathComponent(storeName + ".sqlite")
        return URL(fileURLWithPath: sqliteFilePath)
    }
    
    lazy var storeDescription: NSPersistentStoreDescription = {
        let description = NSPersistentStoreDescription(url: self.storeURL)
        description.shouldMigrateStoreAutomatically = true
        description.shouldInferMappingModelAutomatically = true
        return description
    }()

    func saveContext(){
        guard managedContext.hasChanges else {return}
        do {
            try managedContext.save()
//            postUpdateNotification(Notification.init(name: Notification.Name(rawValue: UpdateScreenNotification)))
        }catch let nserror as NSError{
            print("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    func clearStorage(){
    }
    
    func getBackgroundContext()->NSManagedObjectContext{
        let backgManageContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        backgManageContext.persistentStoreCoordinator = self.managedContext.persistentStoreCoordinator
        backgManageContext.mergePolicy = NSOverwriteMergePolicy
        return backgManageContext
    }
    
    func setupSaveNotification(){
        NotificationCenter.default.addObserver(forName: .NSManagedObjectContextDidSave, object: nil, queue: nil) { (notification) in
            let moc = self.managedContext
            if let notContext = notification.object as? NSManagedObjectContext{
                if (notContext != moc){
                    moc.perform({
                        moc.mergeChanges(fromContextDidSave: notification)
                        self.postUpdateNotification(notification)
                    })
                }
            }
        }
    }
    
    func postUpdateNotification(_ notification: Notification){
         NotificationCenter.default.post(name: notification.name, object: nil, userInfo: notification.userInfo)
//        print("update ui")
    }
}

//MARK: basic methods methods
extension CoreDataStack {
    func get(request: NSFetchRequest<NSFetchRequestResult>) throws ->[Any]?{
        do{
           let result = try managedContext.fetch(request)
           return result
        }catch{
            throw error
        }
        
    }
}

//MARK: Methods for object
extension CoreDataStack {
    func getCountOf<T:NSManagedObject>(_ type: T.Type) -> Int {
        let productRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: T.className)
        var result: Int = 0
        do {
            result = try self.managedContext.count(for: productRequest)
        }catch {
            print("error count product")
        }
        return result
    }
    
    func getAllObjects<T:NSManagedObject>(_ type: T.Type) -> [T]{
        return self.getObjects(type, predicate: nil)
    }
    
    func getObjects<T:NSManagedObject>(_ type: T.Type, predicate: NSPredicate?) -> [T]{
        let productRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: T.className)
        productRequest.predicate = predicate
        productRequest.fetchBatchSize = 50
        var result: [T]?
        do {
            result = try get(request:productRequest as! NSFetchRequest<NSFetchRequestResult>) as? [T]
        } catch {
            print("error on fetch")
        }
        if (result == nil){
            result = [T]()
        }
        return result!
    }
    
    func createObject<T:NSManagedObject>(_ type: T.Type) -> T{
        let newObject = T(context: self.managedContext)
        return newObject
    }
    
    func deleteObject<T:NSManagedObject>(_ object: T){
        self.deleteObjects([object])
        self.saveContext()
    }
    
    func deleteObjects<T:NSManagedObject>(_ objects: [T]){
        for object in objects{
            self.managedContext.delete(object)
        }
        self.saveContext()
    }
    
    
    func deleteAllObjects<T:NSManagedObject>(_ type: T.Type){
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: type.entity().managedObjectClassName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do {
            _ = try managedContext.execute(request)
            self.saveContext()
        } catch let error as NSError {
            print ("DB error \(error)")
        }
    }
}


