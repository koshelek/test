//
//  HTTPApi.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias HttpResult = (_ success: Bool, _ data: AnyObject?) -> Void
typealias HttpResultWithCancel = (_ success: Bool, _ data: AnyObject?, _ cancel: Bool) -> Void

typealias HttpTask = (_ task: URLSessionTask?) -> Void

fileprivate struct Api {
    static let BaseUrl = URL(string: "https://api.openweathermap.org/data/2.5/")!
    func getBaseUrl() -> URL {
        return Api.BaseUrl
    }
    static func addPath(path: String) -> URL{
        if path.contains("http"){
            return URL(string: path)!
        }
        
        return URL(string: Api.BaseUrl.absoluteString + path)!
    }
    
    static func addPath(path: String, params:[String:String]?) -> URL {
        if (params != nil){
            var currentURLStr = "\(self.addPath(path: path))?"
            for (index, param) in params!.enumerated(){
                currentURLStr = "\(currentURLStr)\((index == 0) ? "" : "&" )\(param.key)=\(param.value)"
            }
            return URL(string: currentURLStr.replacingOccurrences(of: " ", with: "%20")) ?? URL(string: "\(self.addPath(path: path))")!
        }else{
            return self.addPath(path: path)
        }
    }
}
//daily?
//APPID=d855e82424d8733be8ebf0dcd73b6b0a
//q=London&units=metric&cnt=7&
struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}

class HttpApi{
    
    // MARK: - Properties
    private static var sharedNetworkManager: HttpApi = {
        let networkManager = HttpApi(baseURL: Api.BaseUrl)
        return networkManager
    }()
    var isConnectedToInternet: Bool {
        get { return Connectivity.isConnectedToInternet }
    }
    
    // MARK: -
    let baseURL: URL

    // Initialization
    private init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    // MARK: - Accessors
    class func shared() -> HttpApi {
        return sharedNetworkManager
    }
    
    // MARK: Base methods
    func get(url: String, _params: [String:String]?, result: @escaping HttpResult)->DataRequest{
        var params = ["APPID":"d855e82424d8733be8ebf0dcd73b6b0a", "units":"metric"]
        if _params != nil {
            params = params.merging(_params!) { $1 }
        }
       
        let path = Api.addPath(path: url, params: params)
        let request = Alamofire.request(path).responseJSON { (dataResponse) in
            let json = try? JSON(data: dataResponse.data!)
            result((dataResponse.response?.statusCode ?? 0) == 200, json as AnyObject)
        }
        return request
    }
    
    func post(url: String, params: [String:Any]?){
        
    }
}
