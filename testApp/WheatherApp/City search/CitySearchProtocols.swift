//
//  CitySearchProtocols.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

protocol CitySearchView:class {
    func show(cities:[City])
}

protocol CitySearchPresenter:DataBaseUpdate {
    init(view: CitySearchView)
    var currentSearchString: String? {get set}
    func searchFor(string: String)
    func selectedCity(_ city:City)
}

