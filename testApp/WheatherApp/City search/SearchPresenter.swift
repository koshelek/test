//
//  SearchPresenter.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class SearchPresenter: CitySearchPresenter {
    
    
    unowned let view: CitySearchView
    var currentSearchString: String?
    var currentRequest: DataRequest?
    var cities:[City]
    var selectedCities: [City] = [City]()

    required init(view: CitySearchView) {
        self.view = view
        self.cities = [City]()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(dbUpdated), name: NSNotification.Name(UpdateScreenNotification), object: nil)
    }

    func selectedCity(_ city: City) {
        city.showWheather = !city.showWheather
        city.priority = 0
        CoreDataStack.shared.saveContext()
        dbUpdated()
        updateSelectedCities()
    }
    
    func updateSelectedCities(){
        let predicate = NSPredicate(format: "\(#keyPath(City.showWheather)) = YES")
        selectedCities = CoreDataStack.shared.getObjects(City.self, predicate: predicate)
        let zeroPriotityCities = selectedCities.filter(){return $0.priority == 0}
        var sortedArr = selectedCities.sorted { (a, b) -> Bool in
            return a.priority > b.priority
        }
        
        for i in 0..<sortedArr.count {
            sortedArr[i].priority = Int16(sortedArr.count - i)
         }
        
        for i in 0..<zeroPriotityCities.count {
            zeroPriotityCities[i].priority = Int16(sortedArr.count + 1)
            sortedArr.insert(zeroPriotityCities[i], at: 0)
        }
        CoreDataStack.shared.saveContext()
    }
    
    func searchFor(string: String) {
        
        showFromDB(request: string)
        if string.isEmpty == false && currentSearchString != string{
            currentRequest = HttpApi.shared().get(url: "find", _params: ["q":string]) { [unowned self] (success, result)  in
                //if result for current search
                if  success == true && result != nil{
                    self.parseResponseFromServer(object: result!)
                }else{
                    print ("error \(String(describing: result))")
                }
            }
        }
        currentSearchString = string
    }
    
    func fetchCitiesFromDB(request: String) -> [City]{
        let db = CoreDataStack.shared
        
        var predicate: NSPredicate? = NSPredicate(format: "%K CONTAINS[cd] %@", #keyPath(City.name),request)
        if request.isEmpty {
            predicate = nil
        }
        let countries = db.getObjects(City.self, predicate: predicate)
        return countries
    }
    
    func showFromDB(request: String){
        let currentResult = fetchCitiesFromDB(request: request)
        cities = currentResult
        self.view.show(cities: cities)
    }
    
    @objc func dbUpdated(){
        self.searchFor(string: currentSearchString ?? "")
    }
}


extension SearchPresenter {
    func parseResponseFromServer(object: AnyObject){
        guard let json = object as? JSON else {
            return
        }
        if json["cod"].intValue == 200 {
            let jsonObjects = json["list"].arrayValue
            Country.fillCountriesFromJsonArray(objects: jsonObjects)
            self.dbUpdated()
        }
        
    }
}


extension Country {
    class func fillCountriesFromJsonArray(objects:[JSON]){
        let db = CoreDataStack.shared
        for object in objects{
            let id = object["id"].stringValue
            let name = object["name"].stringValue
            let iso = object["sys"]["country"].stringValue
            
            let predicate = NSPredicate(format: "\(#keyPath(City.id)) == \(id)")
            var result = db.getObjects(City.self, predicate: predicate).first
            if result == nil {
                result = db.createObject(City.self)
            }
            
            result?.id = id
            result?.name = name
            
            let predicateCountry = NSPredicate(format: "iso = '\(iso)'")
            var resultCountry = db.getObjects(Country.self, predicate: predicateCountry).first
            if resultCountry == nil {
                resultCountry = db.createObject(Country.self)
            }
            resultCountry?.iso = iso
            
            result?.country = resultCountry
        }
        
        db.saveContext()
        
    }
}
