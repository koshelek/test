//
//  WheatherPresenter.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import SDWebImage

class WheatherPresenter:NSObject, WheatherViewPresenter {
    //MARK: Properties
    unowned let view: WheatherView
    var cities: [City]

    
    //MARK: Init
    required init(view: WheatherView, cities: [City]) {
        self.view = view
        self.cities = cities
    
        super.init()
    }
    
    func checkForUpdates(){
        dbUpdated()
        updateWheather()
    }
    
    func dbUpdated() {
        let predicate = NSPredicate(format: "\(#keyPath(City.showWheather)) = YES")
        let cities = CoreDataStack.shared.getObjects(City.self, predicate: predicate).sorted {$0.priority > $1.priority}
        
        self.cities = cities
        self.view.tableView.reloadData()
    }
    
    //Mark: Data Source Protocol
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WheatherTableViewCell.identifier(), for: indexPath) as! WheatherTableViewCell
        let city = self.cities[indexPath.row]
        
        cell.cityNameLabel.text = "\(city.name ?? ""), \(city.country?.iso ?? "NaN")\nid:\(city.id ?? "Nan")"
        
        if let wheathers = (city.wheathers?.allObjects as? [Wheather])?.sorted(by: {($0.date) ?? Date() < $1.date ?? Date()}) {
            if let now = wheathers.first {
                cell.valueLabal.text = "\(Int(now.day))"
                cell.additionalInfo.text = "Night:\(now.night)\nMin:\(now.min)\nMax:\(now.max)"
                cell.windLabel.text = "Wind speed:\(now.windSpeed)\nWeather:\(now.desc ?? "")"
                
                for i in 1..<wheathers.count {
                    let wheather = wheathers[i]
                    cell.forecastDayLabels[i-1].text = "\(Int(wheather.day))"
                    cell.forecastNightLabels[i-1].text = "\(Int(wheather.night))"
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM d, eee"
                    cell.forecastDateLabels[i-1].text = dateFormatter.string(from: wheather.date ?? Date())
                }
            }
        }
        cell.setBlur()
        cell.mainImageBackground.image = nil
        if city.imageUrl != nil {
            cell.mainImageBackground.sd_setImage(with: URL(string: city.imageUrl!), completed: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let tmp = cities[destinationIndexPath.row]
        cities[destinationIndexPath.row] = cities[sourceIndexPath.row]
        cities[sourceIndexPath.row] = tmp
        
        for i in 0..<cities.count {
            cities[i].priority = Int16(cities.count - i)
        }
        CoreDataStack.shared.saveContext()
        dbUpdated()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            cities[indexPath.row].showWheather = false
            cities.remove(at: indexPath.row)
            CoreDataStack.shared.saveContext()
            dbUpdated()
        }
        
    }

}


extension WheatherPresenter{
    func updateWheather(){
        let ids = self.cities.map(){$0.id ?? ""}
        for id in ids {
            if id.isEmpty {
                continue
            }
            _ = HttpApi.shared().get(url: "forecast/daily", _params: ["id" : id, "cnt":"8"], result: { (success, response) in
                guard let json = response as? JSON else {
                    return
                }
                Country.fillForecastFromJson(object: json)
                self.loadImage(cityId: id)
                self.dbUpdated()
            })
        }
    }
    
    func loadImage(cityId: String){
        let predicate = NSPredicate(format: "\(#keyPath(City.id)) = \(cityId)")
        if let city = CoreDataStack.shared.getObjects(City.self, predicate: predicate).first, city.imageUrl == nil {
            _ = HttpApi.shared().get(url: "https://pixabay.com/api/",
                                    _params: ["key" : "10797571-58364db0167462ba6dfbfb342",
                                              "q" : city.name ?? "city",
                                              "orientation" : "horizontal",
                                              "image_type" : "photo",
                                              "per_page" : "3",
                                              "category":"city"],
                                    result: { (success, response) in
                guard let json = response as? JSON else {
                    return
                }
                                        if let image = json["hits"].arrayValue.first {
                                            city.imageUrl = image["webformatURL"].stringValue
                                            CoreDataStack.shared.saveContext()
                                            self.dbUpdated()
                                        }
                                        
                                        
                
            })
        }
    }
}


extension Country {

    class func fillForecastFromJson(object: JSON){
        let cityId = object["city"]["id"].stringValue
        let predicate = NSPredicate(format: "\(#keyPath(City.id)) = \(cityId)")
        let db = CoreDataStack.shared
        if let city = db.getObjects(City.self, predicate: predicate).first {
            city.name = object["city"]["name"].stringValue
            
            let list = object["list"].arrayValue
            var newWeathers = [Wheather]()
            for weather in list {
                let tmpWeather = db.createObject(Wheather.self)
                tmpWeather.city = city
                tmpWeather.date = Date(timeIntervalSince1970: TimeInterval(weather["dt"].intValue))
                tmpWeather.lastUpdate = Date()
                tmpWeather.day = weather["temp"]["day"].floatValue
                tmpWeather.night = weather["temp"]["night"].floatValue
                tmpWeather.min = weather["temp"]["min"].floatValue
                tmpWeather.max = weather["temp"]["max"].floatValue
                tmpWeather.windSpeed = weather["speed"].floatValue
                tmpWeather.desc = weather["weather"].arrayValue.first?["description"].string
                newWeathers.append(tmpWeather)
            }
            city.wheathers = NSSet(array: newWeathers)
            db.saveContext()
        }
    }
}
