//
//  WheatherViewController.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import UIKit

fileprivate let SelfId = "WheatherViewController"

class WheatherViewController: UIViewController, WheatherView, UITableViewDelegate {
    
    var presenter: WheatherViewPresenter?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.allowsSelection = false
        self.tableView.dataSource = self.presenter
        self.tableView.delegate = self


        self.presenter?.checkForUpdates()
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return self.view.frame.height/4.0
//    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CitySearchTableViewController {
            let presenter = SearchPresenter(view: vc)
            vc.presenter = presenter
        }
        
    }

    @IBAction func editAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
    
}

extension WheatherViewController: StoryboardInitable {
    static func initFromStoryboard() -> UIViewController {
        let vc = UIStoryboard(name: startSt, bundle: nil).instantiateViewController(withIdentifier: SelfId) as! WheatherViewController
        return vc
    }
}
