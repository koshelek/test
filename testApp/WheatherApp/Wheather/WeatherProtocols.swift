//
//  WeatherProtocols.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import Foundation
import UIKit

protocol WheatherView:class {
    var tableView:UITableView! {get set}
}

protocol WheatherViewPresenter: DataBaseUpdate, UITableViewDataSource {
    init(view: WheatherView, cities: [City])
    func checkForUpdates()
}
