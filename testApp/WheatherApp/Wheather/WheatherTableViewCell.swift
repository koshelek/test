//
//  WheatherTableViewCell.swift
//  testApp
//
//  Created by Архип on 11/24/18.
//  Copyright © 2018 Arkhyp. All rights reserved.
//

import UIKit

class WheatherTableViewCell: UITableViewCell {
    class SmallWheatherView: UIView {
        @IBOutlet weak var dayBackgroundImageView: UIImageView!
        @IBOutlet weak var dateLabel: UILabel!
        @IBOutlet weak var Night: UIImageView!
    }
    @IBOutlet weak var windLabel: UILabel!
    
    @IBOutlet weak var additionalInfo: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var mainImageBackground: UIImageView!
    @IBOutlet weak var valueLabal: UILabel!
    @IBOutlet weak var measureLabel: UILabel!
    
    @IBOutlet var foreCastViews: [UIView]!
    @IBOutlet var forecastDateLabels: [UILabel]!
    @IBOutlet var forecastDayLabels: [UILabel]!
    @IBOutlet var forecastNightLabels: [UILabel]!
    
    class func identifier() -> String { return "WheatherCell" }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var blur:UIVisualEffectView?
    func setBlur() {
        
        if blur == nil, mainImageBackground != nil {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
//            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            mainImageBackground!.addSubview(blurEffectView)
            blur?.frame = mainImageBackground!.bounds
            blur = blurEffectView
            
            
        }
        blur?.frame = mainImageBackground!.bounds
        blur?.alpha = 0.6
    }

}


